#!/bin/bash
echo "RUNNING DB INIT SCRIPT"
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
    CREATE TABLE case_by_county (
        date DATE NOT NULL,
        county VARCHAR(100) NOT NULL,
        state VARCHAR(100) NOT NULL,
        fips VARCHAR(100),
        cases INT NOT NULL,
        deaths INT NOT NULL,
        PRIMARY KEY (date, county, state));
EOSQL

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
    COPY case_by_county(date,county,state,fips,cases,deaths) 
    FROM '/case-data.txt' DELIMITER ',' CSV HEADER;
EOSQL
