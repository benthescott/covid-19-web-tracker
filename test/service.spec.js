'use strict';

process.env.NODE_ENV = 'test';

let chai = require('chai'),
    chaiHttp = require('chai-http');

let expect = chai.expect;

chai.use(chaiHttp);

const server = require('../src/server');

const cache = require('../src/app/app.cache');

describe('Testing default routes', () => {

    afterAll((done) => {
        server.close(done);
    })

    test('should get a 200 from / route', (done) => {
        chai.request(server)
            .get('/')
            .end((error, response) => {
                expect(error).to.be.null;
                expect(response).to.have.status(200);
                done();
            });
    });

    test('should get a 204 from OPTIONS / route', (done) => {
        chai.request(server)
            .options('/')
            .end((error, response) => {
                expect(error).to.be.null;
                expect(response).to.have.status(204);
                done();
            });
    });

    test('should get a 200 from /healthcheck route', (done) => {
        chai.request(server)
            .get('/healthcheck')
            .end((error, response) => {
                expect(error).to.be.null;
                expect(response).to.have.status(200);
                done();
            });
    });

    test('should get a 204 from OPTIONS /ecv route', (done) => {
        chai.request(server)
            .options('/healthcheck')
            .end((error, response) => {
                expect(error).to.be.null;
                expect(response).to.have.status(204);
                done();
            });
    });

    test('should get a 404 from /bogus route', (done) => {
        chai.request(server)
            .get(`/bogus`)
            .end((error, response) => {
                expect(response).to.have.status(404);
                done();
            });
    });

});

describe('testing tracker routes', () => {
    afterAll((done) => {
        server.close(done);
    })

    test('should give 400 bad request from /tracker/county', (done) => {
        chai.request(server)
            .get(`/tracker/county`)
            .end((error, response) => {
                expect(response).to.have.status(400);
                done();
            });
    });

    test('should give 400 bad request from /tracker/county', (done) => {
        chai.request(server)
            .get(`/tracker/county?foo=bar`)
            .end((error, response) => {
                expect(response).to.have.status(400);
                done();
            });
    });

    // Due to no database connection, will always receive a 502
    test('should give 502 bad gateway from /tracker/county', (done) => {
        chai.request(server)
            .get(`/tracker/date?state=South+Carolina`)
            .end((error, response) => {
                expect(response).to.have.status(502);
                done();
            });
    });

    test('should give 400 bad request from /tracker/date', (done) => {
        chai.request(server)
            .get(`/tracker/county`)
            .end((error, response) => {
                expect(response).to.have.status(400);
                done();
            });
    });

    test('should give 400 bad request from /tracker/date', (done) => {
        chai.request(server)
            .get(`/tracker/date?foo=bar`)
            .end((error, response) => {
                expect(response).to.have.status(400);
                done();
            });
    });

    // Due to no database connection, will always receive a 502
    test('should give 502 bad gateway from /tracker/date', (done) => {
        chai.request(server)
            .get(`/tracker/date?state=Arkansas`)
            .end((error, response) => {
                expect(response).to.have.status(502);
                done();
            });
    });

    test('should give 200 ok from /tracker/date using cache', async (done) => {
        await cache.setAsync('/tracker/date?state=Arkansas', JSON.stringify([{date: 'April 14, 2020, 12:00 AM', cases: 297, deaths: 10}]));
        chai.request(server)
            .get(`/tracker/date?state=Arkansas`)
            .end((error, response) => {
                expect(response).to.have.status(200);
                done();
            });
    });

    test('should give 204 no content from /tracker/date using cache', async (done) => {
        await cache.setAsync('/tracker/date?state=Arkansas', JSON.stringify([]));
        chai.request(server)
            .get(`/tracker/date?state=Arkansas`)
            .end((error, response) => {
                expect(response).to.have.status(204);
                done();
            });
    });

    // Due to no database connection, will always receive a 502
    test('should give 502 bad gateway from /tracker', (done) => {
        chai.request(server)
            .get(`/tracker`)
            .end((error, response) => {
                expect(response).to.have.status(502);
                done();
            });
    });
});
