'use strict';

const CACHE_KEY_1 = 'Hello',
    CACHE_DATA_1 = 'World!';

const CACHE_KEY_2 = 'foo',
    CACHE_DATA_2 = 'bar';

describe('Testing cache', () => {

    let cache;

    beforeEach(() => {
        cache = require('../src/app/app.cache');
    });

    afterEach(() => {
        cache = null;
    })

    test('should set key/value pair correctly', async () => {
        expect.assertions(1);
        await cache.setAsync(CACHE_KEY_1, CACHE_DATA_1);
        const result = await cache.getAsync(CACHE_KEY_1);
        expect(result).toStrictEqual(CACHE_DATA_1);
    });

    test('should get 2 keys', async () => {
        expect.assertions(3);

        await cache.setAsync(CACHE_KEY_1, CACHE_DATA_1);
        await cache.setAsync(CACHE_KEY_2, CACHE_DATA_2);

        const numKeys = await cache.keysAsync(),
            value1 = await cache.getAsync(CACHE_KEY_1),
            value2 = await cache.getAsync(CACHE_KEY_2);

        expect(numKeys.length).toStrictEqual(2);
        expect(value1).toStrictEqual(CACHE_DATA_1);
        expect(value2).toStrictEqual(CACHE_DATA_2);
    });

});