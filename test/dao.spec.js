'use strict';

const db = require('../src/app/app.dao');

const SAMPLE_QUERY_1 = `
    SELECT "public"."case_by_county"."county" AS "county", max("public"."case_by_county"."cases") AS "cases", max("public"."case_by_county"."deaths") AS "deaths"
    FROM "public"."case_by_county"
    WHERE "public"."case_by_county"."state" = 'South Carolina'
    GROUP BY "public"."case_by_county"."county"
    ORDER BY "cases" DESC, "public"."case_by_county"."county" DESC;
`;

const SAMPLE_QUERY_2 = `
    SELECT CAST("public"."case_by_county"."date" AS date) AS "date", max("public"."case_by_county"."cases") AS "cases", max("public"."case_by_county"."deaths") AS "deaths"
    FROM "public"."case_by_county"
    WHERE "public"."case_by_county"."state" = 'New York'
    GROUP BY CAST("public"."case_by_county"."date" AS date)
    ORDER BY CAST("public"."case_by_county"."date" AS date) DESC;
`;

const SAMPLE_QUERY_3 = `
    SELECT "public"."case_by_county"."state" AS "state", max("public"."case_by_county"."cases") AS "cases", max("public"."case_by_county"."deaths") AS "deaths"
    FROM "public"."case_by_county"
    GROUP BY "public"."case_by_county"."state"
    ORDER BY "cases" DESC;
`;

describe('Testing dao', () => {

    afterAll(async () => {
        await db.shutdown();
    });

    test('should throw an exception', async () => {
        expect.assertions(1);
     
        await expect(db.query('SELECT * FROM case_by_county;', [])).rejects.toThrow();
    });

    // In order for this test to work, all white space is removed before the assertion is made.
    test('query should match sample 1', () => {
        expect.assertions(1);
        const query = db.getConfirmedByCountyQuery('South Carolina');
        expect(query.replace(/\s/g,'')).toBe(SAMPLE_QUERY_1.replace(/\s/g,''));
    });

    // In order for this test to work, all white space is removed before the assertion is made.
    test('query should match sample 2', () => {
        expect.assertions(1);
        const query = db.getConfirmedByDateQuery('New York');
        expect(query.replace(/\s/g,'')).toBe(SAMPLE_QUERY_2.replace(/\s/g,''));
    });

    // In order for this test to work, all white space is removed before the assertion is made.
    test('query should match sample 3', () => {
        expect.assertions(1);
        const query = db.getConfirmedByStateQuery();
        expect(query.replace(/\s/g,'')).toBe(SAMPLE_QUERY_3.replace(/\s/g,''));
    });

});