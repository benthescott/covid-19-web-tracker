# Covid-19 Web Tracker

[![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg)](https://github.com/RichardLitt/standard-readme)
[![coverage report](https://gitlab.com/benthescott/covid-19-web-tracker/badges/master/coverage.svg)](https://gitlab.com/benthescott/covid-19-web-tracker/-/commits/master)
[![pipeline status](https://gitlab.com/benthescott/covid-19-web-tracker/badges/master/pipeline.svg)](https://gitlab.com/benthescott/covid-19-web-tracker/-/commits/master)

## Table of Contents

- [Covid-19 Web Tracker](#covid-19-web-tracker)
  - [Table of Contents](#table-of-contents)
  - [Background](#background)
  - [Assumptions](#assumptions)
  - [Usage](#usage)
  - [Maintainers](#maintainers)
  - [Contribute](#contribute)
  - [License](#license)

## Background

This project adds an interactive web application in top of Covid-19 case data from the New York Times located [here](https://github.com/nytimes/covid-19-data/blob/master/us-counties.csv). It is a "full-stack" application utilizing [PostgreSQL](https://www.postgresql.org/docs/12/index.html), [Redis](https://redis.io/) with a [node-cache](https://www.npmjs.com/package/node-cache) fallback cache, NodeJS, and a web front-end.

## Assumptions

This project was written in JavaScript utilizing ES6 syntax and leverages [NodeJS](https://nodejs.org/dist/latest-v12.x/docs/api/) v12.16.1 as a runtime. It is based on the open-minded HTTP framework [Express](https://expressjs.com/). The package manager used was [NPM](https://www.npmjs.com/) v6.13.4. This project was developed entirely in a MacOS environment and **has not** been run in a Windows environmemt.

Utilizing [Docker](https://www.docker.com/) is required to run the entire application locally, although unit tests can be run without Docker. See [Usage](#Usage) for more information. During composition, Postgres is loaded with the NYT Covid-19 case data automatically.

If the application is running, the [Swagger](https://swagger.io/) docs for the web API are located [here](http://localhost:8080/api-docs).

The web app is located [here](http://localhost:8080/home/index.html), assuming the app is running. Data is displayed
when running with Docker.

## Usage

Always run these commands before running this project locally:
```console
$ git clone https://gitlab.com/benthescott/covid-19-web-tracker.git
```
```console
$ cd covid-19-web-tracker
```
```console
$ npm i
```
If you would like to run unit tests without composing the end-to-end application with Docker:
```console
$ npm test
```

This project utilizes [PM2](https://pm2.io/docs/plus/overview/)
as a process manager. Running `$ npm start` automatically uses PM2 with its behavior defined by the ecosystem.config.js file.
If running other PM2 commands are necessary (such as `$ pm2 ls` or `$ pm2 kill`) the local binary can be accessed via `$ npm run pm2 [options] <command>`.

If you would like to run the end-to-end application, you can use Docker:
```console
$ npm run docker
```
> Due to the way the [offical](https://hub.docker.com/_/postgres) Postgres Docker images work, they *only* run the init scripts in the directory mounted to `/docker-entrypoint-initdb.d` if the data directory is empty (in this case, `/db` and `/covid-db-data` respectively). If the database requires a refresh, simply run `$ npm run clean` before `$ npm run docker`.

## Maintainers

- @Benthescott

## Contribute

Submit issues and PRs.

## License

© Benjamin Cline
