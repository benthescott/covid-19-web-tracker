'use strict';

const router = require('express').Router();
const trackerController = require('../controllers/controller.tracker');

router.get('/tracker', trackerController.getCachedResponse,
    trackerController.getQuery, trackerController.getCovidCaseData);

router.get('/tracker/date', trackerController.isQueryValid, trackerController.getCachedResponse, 
    trackerController.getQuery, trackerController.getCovidCaseData);
    
router.get('/tracker/county', trackerController.isQueryValid, trackerController.getCachedResponse,
    trackerController.getQuery, trackerController.getCovidCaseData);

module.exports = router;