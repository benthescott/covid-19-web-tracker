/**
 * Default routes
 */

'use strict';

const router = require('express').Router();
const defaultController = require('../controllers/controller.default');

// Healthcheck route for the cloud LB
router.get('/', defaultController.ecv);
router.get('/healthcheck', defaultController.ecv);

module.exports = router;