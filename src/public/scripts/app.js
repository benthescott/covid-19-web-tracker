let app = new Vue({
    el: '#app',
    data: {
        countyCases: [],
        dateCases: [],
        stateCases: [],
        currentState: "South Carolina"
    },
    methods: {
        loadCountyData: async function() {
            let response = await fetch(`/tracker/county?state=${encodeURI(this.currentState)}`);
            this.countyCases = await response.json();
        },
        loadDateData: async function() {
            let response = await fetch(`/tracker/date?state=${encodeURI(this.currentState)}`);
            this.dateCases = await response.json();
        },
        loadStateData: async function() {
            let response = await fetch(`/tracker`);
            this.stateCases = await response.json();
        },
        updateCaseData: async function() {
            await this.loadCountyData();
            await this.loadDateData();
        },
        sortDatesDesc: function() {
            this.dateCases = this.dateCases.sort((a, b) => {
                return new Date(b.date) - new Date(a.date);
            });
        },
        sortCountyCasesDesc: function() {
            this.countyCases = this.countyCases.sort((a, b) => {
                return b.cases - a.cases;
            });
        },
        sortStateCasesDesc: function() {
            this.stateCases = this.stateCases.sort((a, b) => {
                return b.cases - a.cases;
            });
        },
        sortCountyDeathsDesc: function() {
            this.countyCases = this.countyCases.sort((a, b) => {
                return b.deaths - a.deaths;
            });
        },
        sortStateDeathsDesc: function() {
            this.stateCases = this.stateCases.sort((a, b) => {
                return b.deaths - a.deaths;
            });
        },
        sortDatesAsc: function() {
            this.dateCases = this.dateCases.sort((a, b) => {
                return new Date(a.date) - new Date(b.date);
            });
        },
        sortCountyCasesAsc: function() {
            this.countyCases = this.countyCases.sort((a, b) => {
                return a.cases - b.cases;
            });
        },
        sortStateCasesAsc: function() {
            this.stateCases = this.stateCases.sort((a, b) => {
                return a.cases - b.cases;
            });
        },
        sortCountyDeathsAsc: function() {
            this.countyCases = this.countyCases.sort((a, b) => {
                return a.deaths - b.deaths;
            });
        },
        sortStateDeathsAsc: function() {
            this.stateCases = this.stateCases.sort((a, b) => {
                return a.deaths - b.deaths;
            });
        },
    },
    filters: {
        formatDate: function(date) {
            if (!date)
                return '';
            return date.split('T')[0];
        }
    },
    created: async function() {
        await this.updateCaseData();
        await this.loadStateData();
    }
});