'use strict';

module.exports = (() => {
    return {
        PORT: 8080,
        TIMEZONE: 'America/Chicago',
        LOG_DIR: './log',
        MORGAN_TEMPLATE: ':customTimestamp :http-version :method :remote-addr :response-time :status :url :port',
        HTTP_OK: 200,
        HTTP_NO_CONTENT: 204,
        HTTP_BAD_REQUEST: 400,
        HTTP_NOT_FOUND: 404,
        HTTP_SERVER_ERROR: 500,
        HTTP_BAD_GATEWAY: 502,
    };
})();