'use strict';

const Express = require('express'),
    Constants  = require('./util/constants'),
    Morgan = require('morgan'),
    compression = require('compression'),
    cors = require('cors'),
    swaggerUI = require('swagger-ui-express'),
    swaggerDoc = require('./swagger.json'),
    path = require('path'),
    favicon = require('serve-favicon');

// Logging class wrapper for Winston
const StreamLog = require("./util/logger").StreamLog;
const Log = require("./util/logger").Log;

// Set up HTTP request logging via Morgan
const defaultController = require('./controllers/controller.default');

Morgan.token('port', (req) => {
    return req.port
});

Morgan.token('customTimestamp', (req) => {
    return req.customTimestamp
});

const app = Express();

app.use(defaultController.assignPort);
app.use(defaultController.assignDate);

app.use(Morgan(Constants.MORGAN_TEMPLATE, { stream: StreamLog.stream }));

// Auto-parse application/json
app.use(Express.json());

// Add gzip compression
app.use(compression());

app.use('/home', Express.static(path.join(__dirname, 'public')));
app.use(favicon(path.join(__dirname, 'public/images', 'favicon.ico')));

// Allow CORs and complex pre-flight requests
app.use(cors());
app.options('*', cors());

app.use('/api-docs', swaggerUI.serve, swaggerUI.setup(swaggerDoc));

// Load routes
const defaultRoutes = require('./routes/routes.default'),
    trackerRoutes = require('./routes/routes.tracker');

app.use(defaultRoutes);
app.use(trackerRoutes);

app.use((req, res) => {
    res.status(Constants.HTTP_NOT_FOUND).send();
});

// Start server
if (process.env.NODE_ENV !== 'test') {
    app.listen(Constants.PORT, () => {
        Log.info(`Server has started on port ${Constants.PORT}`);
    });
}

module.exports = app;