'use strict';

const { Pool } = require('pg'),
    { Log } = require('../util/logger');

const pool = new Pool({
    host: process.env.POSTGRES_HOST,
    user: process.env.POSTGRES_USER,
    password: process.env.POSTGRES_PASSWORD,
    database: process.env.POSTGRES_DB,
});

/**
 * Simplistic but robust Postgres module utilizing the official driver. Creates a connection pool and only runs select queries while 
 * handling and bubbling up exceptions when necessary.
 */
module.exports = (() => {

    /**
     * Given a state, returns an SQL query for selecting total cases and deaths grouped by counties
     * for the given state.
     * 
     * @param {string} state 
     */
    function getConfirmedByCountyQuery(state) {
        return `
            SELECT "public"."case_by_county"."county" AS "county", max("public"."case_by_county"."cases") AS "cases", max("public"."case_by_county"."deaths") AS "deaths"
            FROM "public"."case_by_county"
            WHERE "public"."case_by_county"."state" = '${state}'
            GROUP BY "public"."case_by_county"."county"
            ORDER BY "cases" DESC, "public"."case_by_county"."county" DESC;
        `;
    }

    /**
     * Given a state, returns an SQL query for selecting total cases and deaths grouped by date
     * for the given state.
     * 
     * @param {string} state 
     */
    function getConfirmedByDateQuery(state) {
        return `
            SELECT CAST("public"."case_by_county"."date" AS date) AS "date", max("public"."case_by_county"."cases") AS "cases", max("public"."case_by_county"."deaths") AS "deaths"
            FROM "public"."case_by_county"
            WHERE "public"."case_by_county"."state" = '${state}'
            GROUP BY CAST("public"."case_by_county"."date" AS date)
            ORDER BY CAST("public"."case_by_county"."date" AS date) DESC;
        `;
    }

    /**
     * Returns an SQL query for selecting total cases and deaths grouped by state for the US.
     * 
     */
    function getConfirmedByStateQuery() {
        return `
            SELECT "public"."case_by_county"."state" AS "state", max("public"."case_by_county"."cases") AS "cases", max("public"."case_by_county"."deaths") AS "deaths"
            FROM "public"."case_by_county"
            GROUP BY "public"."case_by_county"."state"
            ORDER BY "cases" DESC;
        `;
    }

    /**
     * This function accepts an SQL query with optional parameters and runs the query on the database.
     * 
     * @param {string} text 
     * @param {array} params 
     */
    async function query(text, params) {
        try {
            const client = await pool.connect();
            try {
                const result = await client.query(text, params);
                Log.info(`query ${result.command} resulted in ${result.rowCount} rows`);
                return result;

            } catch (e) {
                Log.error(`An exception occurred during query ${text} with parameters ${params}`);
                Log.error(e.stack);
                throw e;

            } finally {
                client.release()
            }
        } catch(e) {
            Log.error(`An exception occurred while attempting to get database connection`);
            Log.error(e.stack);
            throw e;

        }
    }

    /**
     * This function shuts down the connection pool gracefully
     */
    async function shutdown() {
        return await pool.end();
    }

    return {
        query: query,
        shutdown: shutdown,
        getConfirmedByCountyQuery: getConfirmedByCountyQuery,
        getConfirmedByDateQuery: getConfirmedByDateQuery,
        getConfirmedByStateQuery: getConfirmedByStateQuery
    }

})();