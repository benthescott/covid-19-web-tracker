'use strict';

const redis = require('redis');
const {promisify} = require('util');
const Log = require('../util/logger').Log;

/**
 * This is a wrapper class around two node modules: 'redis' and 'node-cache'. The purpose of this class is to provide a
 * module with two caching strategies while keeping the same API. If using the provided Docker environment, this app will
 * use Redis to cache API responses. Otherwise, it will fallback to node-cache, an in-memory cache isolated to each app instance.
 * The advantage to using Redis is that all app instances can share the same cache, which is essential when a node app is
 * clustered.
 *
 */
class Cache {
    constructor() {
        try {
            if (process.env.HOST_ENV === 'remote') {
                const client = redis.createClient({host: process.env.REDIS_HOST});

                client.on('ready', () => {
                    Log.info('Redis client connected');
                });

                Object.assign(this, {
                    ...client,
                    getAsync: promisify(client.get).bind(client),
                    setAsync: promisify(client.set).bind(client),
                    keysAsync: promisify(client.keys).bind(client)
                });

                this.fallback = false;
            } else {
                throw new Error("Can't connect to Redis, not running in remote env");
            }
        } catch(error) {
            Log.warn('Error constructing Redis client; creating fallback cache');
            const NodeCache = require( "node-cache" );
            const client = new NodeCache({enableLegacyCallbacks: true});

            Object.assign(this, {
                ...client,
                getAsync: promisify(client.get).bind(client),
                setAsync: promisify(client.set).bind(client),
                keysAsync: promisify(client.keys).bind(client)
            });

            this.fallback = true;
        }

        Object.seal(this);
    }
}

module.exports = new Cache();