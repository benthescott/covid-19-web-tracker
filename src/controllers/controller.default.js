'use strict';

const Constants = require('../util/constants');
const Moment = require('moment-timezone');

/**
 * Middleware for assigning values to the API access logs via Morgan, and generic middleware.
 *
 * @type {object}
 */
module.exports = (() => {

    function assignPort (req, res, next) {
        req.port = Constants.PORT;
        next()
    }

    function assignDate(req, res, next) {
        req.customTimestamp = Moment().tz(Constants.TIMEZONE).format();
        next();
    }

    function serviceHealth(req, res, next) {
        res.status(Constants.HTTP_OK).send({"message": "covid-19-web-tracker is healthy."})
    }

    return {
        assignPort: assignPort,
        assignDate: assignDate,
        ecv: serviceHealth
    };

})();