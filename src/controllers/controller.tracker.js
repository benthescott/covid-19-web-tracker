'use strict';

const constants = require('../util/constants'),
    db = require('../app/app.dao'),
    cache = require('../app/app.cache'),
    Log = require('../util/logger').Log;

module.exports = (() => {

    /**
     * This function grabs the request-scoped SQL query, executes the query against the database, caches the result
     * and returns the response to the client.
     * 
     * @param {object} req 
     * @param {object} res 
     * @param {function} next 
     */
    async function getCovidCaseData(req, res, next) {
        const sqlQuery = res.locals.sql;
        try {
            const data = await db.query(sqlQuery);
            await cache.setAsync(req.originalUrl, JSON.stringify(data.rows));
            res.status(constants.HTTP_OK).send(data.rows);
        } catch(e) {
            Log.error('Error while retrieving data from database', e.stack);
            res.status(constants.HTTP_BAD_GATEWAY).send();
        }
    }

    /**
     * This middleware function determines which query must be run for the specified resource and stores it in res.locals.
     * 
     * @param {object} req 
     * @param {object} res 
     * @param {function} next 
     */
    function getQuery(req, res, next) {
        const state = req.query.state,
            path = req.originalUrl;

        if (state) {
            if (path.split('/').pop().split('?').indexOf('date') === -1) {
                res.locals.sql = db.getConfirmedByCountyQuery(state);
            } else {
                res.locals.sql = db.getConfirmedByDateQuery(state);
            }
        } else {
            res.locals.sql = db.getConfirmedByStateQuery();
        }

        next();
    }

    /**
     * Caching middleware. This function looks in the app cache to see if the response is there. If so, sends the repsonse. if not,
     * continues to the next function in the chain.
     * 
     * @param {object} req 
     * @param {object} res 
     * @param {function} next 
     */
    async function getCachedResponse(req, res, next) {
        try {
            const cachedResult = await cache.getAsync(req.originalUrl);
            if (cachedResult !== null && typeof cachedResult !== 'undefined') {
                Log.info(`Responding with cached result for request ${req.originalUrl}`);
                const jsonPayload = JSON.parse(cachedResult);
                if (jsonPayload.length <= 0) {
                    res.status(constants.HTTP_NO_CONTENT).send(jsonPayload);
                } else {
                    res.status(constants.HTTP_OK).send(jsonPayload);
                }
            } else {
                Log.info(`Response not in cache for request ${req.originalUrl}`);
                next();
            }
        } catch(e) {
            Log.error('Error while retrieving cached responses', e.stack);
            next()
        }
    }

    /**
     * This function validates whether the required query param is present on the request
     * 
     * @param {object} req 
     * @param {object} res 
     * @param {function} next 
     */
    function isQueryValid(req, res, next) {
        const state = req.query.state;

        if (typeof state === 'undefined') {
            res.status(constants.HTTP_BAD_REQUEST).send();
        } else {
            next();
        }
    }

    return {
        getCovidCaseData: getCovidCaseData,
        isQueryValid: isQueryValid,
        getCachedResponse: getCachedResponse,
        getQuery: getQuery
    };

})();